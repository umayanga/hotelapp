<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use Request;
use Auth;
use DB;
use App\Models\Hotel;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HotelController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //

        $user_id = Auth::user()->id;
        $hotel_id = DB::select(DB::raw("SELECT hotelId FROM user_role WHERE userId ='+$user_id+';"));


        if ($hotel_id != []) {
            $h_id = $hotel_id[0]->hotelId;
            $view_no;
            if ($hotel_id[0]->hotelId != null) {
                //$hotels = Hotel::where('id', '=', $h_id)->get();
                $view_no = 1;          
                $hotel = Hotel::find($h_id);
                //dd($hotel);
                return view('pages.hotel_details.hotel_show', compact('hotel'));
            } else {
                $hotels = Hotel::all();
                $view_no = 2;
            }
        } else {
            $hotels = Hotel::all();
            $view_no = 2;
        }




        return view('pages.hotel_details.hotel_details_page', compact('hotels', 'view_no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('pages.hotel_details.hotel_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $hotels = Request::all();
        Hotel::create($hotels);
        return redirect('hotel_page');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $hotel = Hotel::find($id);
        return view('pages.hotel_details.hotel_show', compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $hotel = Hotel::find($id);
        return view('pages.hotel_details.hotel_update', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $hotelUpdate = Request::all();
        $hotel = Hotel::find($id);
        $hotel->update($hotelUpdate);
        return redirect('hotel_page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        Hotel::find($id)->delete();
        return redirect('hotel_page');
    }

}
