@extends('layouts.app')

@section('slide_bar')
@include('layouts.system_setting_slide_bar')
@endsection

@section('content')

<br/>

<!-- Main content -->

<section class="content-fluid">
    <div class="box">
        <div class="box-header">
            This section only can Access Authorised  Persons
        </div>
        <div class="box-body">
            <img src="{{ asset('dist/img/access_denied.gif') }}" class="user-image" alt="Access Denied"/>                  
        </div><!-- /.box-body -->
       
    </div>
</div><!-- /.box -->

</section><!-- /.content -->



@endsection

