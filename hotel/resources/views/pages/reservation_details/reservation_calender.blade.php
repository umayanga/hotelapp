@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')


<section class="content-header">
    <h1>Calendar</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Booking</a></li>
        <li class="active">Calendar</li>
    </ol>
</section>
<!-- Main content -->
<section class="content left width_100 fluid">
    <div class="box left box-warning">
        <div class="box-body table-responsive left width_100">
            <div id="cal-container" class="dataTables_wrapper form-inline" role="grid">
                <div class="overlay"></div>
                <div class="loading-img"></div>
                <link rel="stylesheet" href="https://gzscripts.com/AvailabilityBookingCalendarPHP/application/helpers/Calendar/css/style.css" />
                <br />
                <div class="gz-cal-head-row">
                    <div class="float_right summary-nav">
                        <a href="#" class="gz-summary-link-month btn btn-default btn-flat" rel="03-10-15-1"> <i class="fa fa-caret-left"></i> </a>  
                        <a href="#" class="gz-summary-link-month btn btn-default btn-flat" rel="03-12-15-1"> <i class="fa fa-caret-right"></i> </a>
                    </div>
                </div>

                <div class="gz-calendars">
                    <div class="gz-calendars-head">
                        &nbsp;
                    </div>
                    <div class="gz-cal-title">
                        <div class="view view-tenth">
                            <img src='https://gzscripts.com/AvailabilityBookingCalendarPHP/application/web/upload/calendars/thumb/1424809487.jpg' class='preview'>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="gz-cal-title">
                        <div class="view view-tenth">
                            <img src='https://gzscripts.com/AvailabilityBookingCalendarPHP/application/web/upload/calendars/thumb/1424809985.jpg' class='preview'>
                            <div class="mask">
                                <a href="https://gzscripts.com/AvailabilityBookingCalendarPHP/GzCalendar/edit/3" class=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="gz-cal-title">
                        <div class="view view-tenth">
                            <img src='https://gzscripts.com/AvailabilityBookingCalendarPHP/application/web/upload/calendars/thumb/1424810857.jpg' class='preview'>
                            <div class="mask">
                                <a href="https://gzscripts.com/AvailabilityBookingCalendarPHP/GzCalendar/edit/4" class=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="gz-cal-title">
                        <div class="view view-tenth">
                            <img src='https://gzscripts.com/AvailabilityBookingCalendarPHP/application/web/upload/calendars/thumb/1424811582.jpg' class='preview'>
                            <div class="mask">
                                <a href="https://gzscripts.com/AvailabilityBookingCalendarPHP/GzCalendar/edit/5" class=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="gz-cal-title">
                        <div class="view view-tenth">
                            <img src='https://gzscripts.com/AvailabilityBookingCalendarPHP/application/web/upload/calendars/thumb/1436285572.jpg' class='preview'>
                            <div class="mask">
                                <a href="https://gzscripts.com/AvailabilityBookingCalendarPHP/GzCalendar/edit/16" class=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gz-cal-dates">
                    <div class="gz-cal-scroll" style="width: 4636px">
                        <div class="gz-cal-head">
                            <div class="gz-cal-head-row">
                                <span class="month-head-span" style="width: 2279px">Nov 2015</span>
                                <span class="month-head-span" style="width: 2355px">Dec 2015</span>
                            </div>
                            <div class="gz-cal-head-row">
                                <span>
                                    01<br />
                                    Sun                    </span>
                                <span>
                                    02<br />
                                    Mon                    </span>
                                <span>
                                    03<br />
                                    Tue                    </span>
                                <span>
                                    04<br />
                                    Wed                    </span>
                                <span>
                                    05<br />
                                    Thu                    </span>
                                <span>
                                    06<br />
                                    Fri                    </span>
                                <span>
                                    07<br />
                                    Sat                    </span>
                                <span>
                                    08<br />
                                    Sun                    </span>
                                <span>
                                    09<br />
                                    Mon                    </span>
                                <span>
                                    10<br />
                                    Tue                    </span>
                                <span>
                                    11<br />
                                    Wed                    </span>
                                <span>
                                    12<br />
                                    Thu                    </span>
                                <span>
                                    13<br />
                                    Fri                    </span>
                                <span>
                                    14<br />
                                    Sat                    </span>
                                <span>
                                    15<br />
                                    Sun                    </span>
                                <span>
                                    16<br />
                                    Mon                    </span>
                                <span>
                                    17<br />
                                    Tue                    </span>
                                <span>
                                    18<br />
                                    Wed                    </span>
                                <span>
                                    19<br />
                                    Thu                    </span>
                                <span>
                                    20<br />
                                    Fri                    </span>
                                <span>
                                    21<br />
                                    Sat                    </span>
                                <span>
                                    22<br />
                                    Sun                    </span>
                                <span>
                                    23<br />
                                    Mon                    </span>
                                <span>
                                    24<br />
                                    Tue                    </span>
                                <span>
                                    25<br />
                                    Wed                    </span>
                                <span>
                                    26<br />
                                    Thu                    </span>
                                <span>
                                    27<br />
                                    Fri                    </span>
                                <span>
                                    28<br />
                                    Sat                    </span>
                                <span>
                                    29<br />
                                    Sun                    </span>
                                <span>
                                    30<br />
                                    Mon                    </span>
                                <span>
                                    01<br />
                                    Tue                    </span>
                                <span>
                                    02<br />
                                    Wed                    </span>
                                <span>
                                    03<br />
                                    Thu                    </span>
                                <span>
                                    04<br />
                                    Fri                    </span>
                                <span>
                                    05<br />
                                    Sat                    </span>
                                <span>
                                    06<br />
                                    Sun                    </span>
                                <span>
                                    07<br />
                                    Mon                    </span>
                                <span>
                                    08<br />
                                    Tue                    </span>
                                <span>
                                    09<br />
                                    Wed                    </span>
                                <span>
                                    10<br />
                                    Thu                    </span>
                                <span>
                                    11<br />
                                    Fri                    </span>
                                <span>
                                    12<br />
                                    Sat                    </span>
                                <span>
                                    13<br />
                                    Sun                    </span>
                                <span>
                                    14<br />
                                    Mon                    </span>
                                <span>
                                    15<br />
                                    Tue                    </span>
                                <span>
                                    16<br />
                                    Wed                    </span>
                                <span>
                                    17<br />
                                    Thu                    </span>
                                <span>
                                    18<br />
                                    Fri                    </span>
                                <span>
                                    19<br />
                                    Sat                    </span>
                                <span>
                                    20<br />
                                    Sun                    </span>
                                <span>
                                    21<br />
                                    Mon                    </span>
                                <span>
                                    22<br />
                                    Tue                    </span>
                                <span>
                                    23<br />
                                    Wed                    </span>
                                <span>
                                    24<br />
                                    Thu                    </span>
                                <span>
                                    25<br />
                                    Fri                    </span>
                                <span>
                                    26<br />
                                    Sat                    </span>
                                <span>
                                    27<br />
                                    Sun                    </span>
                                <span>
                                    28<br />
                                    Mon                    </span>
                                <span>
                                    29<br />
                                    Tue                    </span>
                                <span>
                                    30<br />
                                    Wed                    </span>
                                <span>
                                    31<br />
                                    Thu                    </span>
                            </div>
                        </div>
                        <div class="gz-cal-program" id="gz-cal-program-1">
                            <a href="" class="calendarReserved" rev="1446336000" rel="1">
                                01                    </a>
                            <a href="" class="calendarReserved" rev="1446422400" rel="1">
                                02                    </a>
                            <a href="" class="calendarReserved" rev="1446508800" rel="1">
                                03                    </a>
                            <a href="" class="calendarReserved" rev="1446595200" rel="1">
                                04                    </a>
                            <a href="" class="calendarReserved" rev="1446681600" rel="1">
                                05                    </a>
                            <a href="" class="calendarReserved" rev="1446768000" rel="1">
                                06                    </a>
                            <a href="" class="calendarReserved" rev="1446854400" rel="1">
                                07                    </a>
                            <a href="" class="calendarReservedNightsEnd" rev="1446940800" rel="1">
                                08                    </a>
                            <a href="" class="calendar" rev="1447027200" rel="1">
                                09                    </a>
                            <a href="" class="calendar" rev="1447113600" rel="1">
                                10                    </a>
                            <a href="" class="calendar" rev="1447200000" rel="1">
                                11                    </a>
                            <a href="" class="calendar" rev="1447286400" rel="1">
                                12                    </a>
                            <a href="" class="calendar" rev="1447372800" rel="1">
                                13                    </a>
                            <a href="" class="calendar" rev="1447459200" rel="1">
                                14                    </a>
                            <a href="" class="calendarReservedNightsStart" rev="1447545600" rel="1">
                                15                    </a>
                            <a href="" class="calendarReserved" rev="1447632000" rel="1">
                                16                    </a>
                            <a href="" class="calendarReserved" rev="1447718400" rel="1">
                                17                    </a>
                            <a href="" class="calendarReserved" rev="1447804800" rel="1">
                                18                    </a>
                            <a href="" class="calendarReserved" rev="1447891200" rel="1">
                                19                    </a>
                            <a href="" class="calendarReserved" rev="1447977600" rel="1">
                                20                    </a>
                            <a href="" class="calendarReservedNightsEnd" rev="1448064000" rel="1">
                                21                    </a>
                            <a href="" class="calendar" rev="1448150400" rel="1">
                                22                    </a>
                            <a href="" class="calendar" rev="1448236800" rel="1">
                                23                    </a>
                            <a href="" class="calendar" rev="1448323200" rel="1">
                                24                    </a>
                            <a href="" class="calendar" rev="1448409600" rel="1">
                                25                    </a>
                            <a href="" class="calendar" rev="1448496000" rel="1">
                                26                    </a>
                            <a href="" class="calendar" rev="1448582400" rel="1">
                                27                    </a>
                            <a href="" class="calendarReservedNightsStart" rev="1448668800" rel="1">
                                28                    </a>
                            <a href="" class="calendarReserved" rev="1448755200" rel="1">
                                29                    </a>
                            <a href="" class="calendarReserved" rev="1448841600" rel="1">
                                30                    </a>
                            <a href="" class="calendarReserved" rev="1448928000" rel="1">
                                01                    </a>
                            <a href="" class="calendarReserved" rev="1449014400" rel="1">
                                02                    </a>
                            <a href="" class="calendarReserved" rev="1449100800" rel="1">
                                03                    </a>
                            <a href="" class="calendarReserved" rev="1449187200" rel="1">
                                04                    </a>
                            <a href="" class="calendarReserved" rev="1449273600" rel="1">
                                05                    </a>
                            <a href="" class="calendarReservedNightsEnd" rev="1449360000" rel="1">
                                06                    </a>
                            <a href="" class="calendar" rev="1449446400" rel="1">
                                07                    </a>
                            <a href="" class="calendar" rev="1449532800" rel="1">
                                08                    </a>
                            <a href="" class="calendar" rev="1449619200" rel="1">
                                09                    </a>
                            <a href="" class="calendar" rev="1449705600" rel="1">
                                10                    </a>
                            <a href="" class="calendar" rev="1449792000" rel="1">
                                11                    </a>
                            <a href="" class="calendar" rev="1449878400" rel="1">
                                12                    </a>
                            <a href="" class="calendar" rev="1449964800" rel="1">
                                13                    </a>
                            <a href="" class="calendar" rev="1450051200" rel="1">
                                14                    </a>
                            <a href="" class="calendar" rev="1450137600" rel="1">
                                15                    </a>
                            <a href="" class="calendar" rev="1450224000" rel="1">
                                16                    </a>
                            <a href="" class="calendar" rev="1450310400" rel="1">
                                17                    </a>
                            <a href="" class="calendar" rev="1450396800" rel="1">
                                18                    </a>
                            <a href="" class="calendar" rev="1450483200" rel="1">
                                19                    </a>
                            <a href="" class="calendar" rev="1450569600" rel="1">
                                20                    </a>
                            <a href="" class="calendar" rev="1450656000" rel="1">
                                21                    </a>
                            <a href="" class="calendar" rev="1450742400" rel="1">
                                22                    </a>
                            <a href="" class="calendar" rev="1450828800" rel="1">
                                23                    </a>
                            <a href="" class="calendar" rev="1450915200" rel="1">
                                24                    </a>
                            <a href="" class="calendar" rev="1451001600" rel="1">
                                25                    </a>
                            <a href="" class="calendar" rev="1451088000" rel="1">
                                26                    </a>
                            <a href="" class="calendar" rev="1451174400" rel="1">
                                27                    </a>
                            <a href="" class="calendar" rev="1451260800" rel="1">
                                28                    </a>
                            <a href="" class="calendar" rev="1451347200" rel="1">
                                29                    </a>
                            <a href="" class="calendar" rev="1451433600" rel="1">
                                30                    </a>
                            <a href="" class="calendar" rev="1451520000" rel="1">
                                31                    </a>
                        </div>
                        <div class="gz-cal-program" id="gz-cal-program-3">
                            <a href="" class="calendar" rev="1446336000" rel="3">
                                01                    </a>
                            <a href="" class="calendar" rev="1446422400" rel="3">
                                02                    </a>
                            <a href="" class="calendar" rev="1446508800" rel="3">
                                03                    </a>
                            <a href="" class="calendar" rev="1446595200" rel="3">
                                04                    </a>
                            <a href="" class="calendar" rev="1446681600" rel="3">
                                05                    </a>
                            <a href="" class="calendar" rev="1446768000" rel="3">
                                06                    </a>
                            <a href="" class="calendar" rev="1446854400" rel="3">
                                07                    </a>
                            <a href="" class="calendar" rev="1446940800" rel="3">
                                08                    </a>
                            <a href="" class="calendar" rev="1447027200" rel="3">
                                09                    </a>
                            <a href="" class="calendar" rev="1447113600" rel="3">
                                10                    </a>
                            <a href="" class="calendar" rev="1447200000" rel="3">
                                11                    </a>
                            <a href="" class="calendar" rev="1447286400" rel="3">
                                12                    </a>
                            <a href="" class="calendar" rev="1447372800" rel="3">
                                13                    </a>
                            <a href="" class="calendar" rev="1447459200" rel="3">
                                14                    </a>
                            <a href="" class="calendar" rev="1447545600" rel="3">
                                15                    </a>
                            <a href="" class="calendar" rev="1447632000" rel="3">
                                16                    </a>
                            <a href="" class="calendar" rev="1447718400" rel="3">
                                17                    </a>
                            <a href="" class="calendar" rev="1447804800" rel="3">
                                18                    </a>
                            <a href="" class="calendar" rev="1447891200" rel="3">
                                19                    </a>
                            <a href="" class="calendar" rev="1447977600" rel="3">
                                20                    </a>
                            <a href="" class="calendar" rev="1448064000" rel="3">
                                21                    </a>
                            <a href="" class="calendar" rev="1448150400" rel="3">
                                22                    </a>
                            <a href="" class="calendar" rev="1448236800" rel="3">
                                23                    </a>
                            <a href="" class="calendar" rev="1448323200" rel="3">
                                24                    </a>
                            <a href="" class="calendar" rev="1448409600" rel="3">
                                25                    </a>
                            <a href="" class="calendar" rev="1448496000" rel="3">
                                26                    </a>
                            <a href="" class="calendar" rev="1448582400" rel="3">
                                27                    </a>
                            <a href="" class="calendar" rev="1448668800" rel="3">
                                28                    </a>
                            <a href="" class="calendar" rev="1448755200" rel="3">
                                29                    </a>
                            <a href="" class="calendar" rev="1448841600" rel="3">
                                30                    </a>
                            <a href="" class="calendar" rev="1448928000" rel="3">
                                01                    </a>
                            <a href="" class="calendar" rev="1449014400" rel="3">
                                02                    </a>
                            <a href="" class="calendar" rev="1449100800" rel="3">
                                03                    </a>
                            <a href="" class="calendar" rev="1449187200" rel="3">
                                04                    </a>
                            <a href="" class="calendar" rev="1449273600" rel="3">
                                05                    </a>
                            <a href="" class="calendar" rev="1449360000" rel="3">
                                06                    </a>
                            <a href="" class="calendar" rev="1449446400" rel="3">
                                07                    </a>
                            <a href="" class="calendar" rev="1449532800" rel="3">
                                08                    </a>
                            <a href="" class="calendar" rev="1449619200" rel="3">
                                09                    </a>
                            <a href="" class="calendar" rev="1449705600" rel="3">
                                10                    </a>
                            <a href="" class="calendar" rev="1449792000" rel="3">
                                11                    </a>
                            <a href="" class="calendar" rev="1449878400" rel="3">
                                12                    </a>
                            <a href="" class="calendar" rev="1449964800" rel="3">
                                13                    </a>
                            <a href="" class="calendar" rev="1450051200" rel="3">
                                14                    </a>
                            <a href="" class="calendar" rev="1450137600" rel="3">
                                15                    </a>
                            <a href="" class="calendar" rev="1450224000" rel="3">
                                16                    </a>
                            <a href="" class="calendar" rev="1450310400" rel="3">
                                17                    </a>
                            <a href="" class="calendar" rev="1450396800" rel="3">
                                18                    </a>
                            <a href="" class="calendar" rev="1450483200" rel="3">
                                19                    </a>
                            <a href="" class="calendar" rev="1450569600" rel="3">
                                20                    </a>
                            <a href="" class="calendar" rev="1450656000" rel="3">
                                21                    </a>
                            <a href="" class="calendar" rev="1450742400" rel="3">
                                22                    </a>
                            <a href="" class="calendar" rev="1450828800" rel="3">
                                23                    </a>
                            <a href="" class="calendar" rev="1450915200" rel="3">
                                24                    </a>
                            <a href="" class="calendar" rev="1451001600" rel="3">
                                25                    </a>
                            <a href="" class="calendar" rev="1451088000" rel="3">
                                26                    </a>
                            <a href="" class="calendar" rev="1451174400" rel="3">
                                27                    </a>
                            <a href="" class="calendar" rev="1451260800" rel="3">
                                28                    </a>
                            <a href="" class="calendar" rev="1451347200" rel="3">
                                29                    </a>
                            <a href="" class="calendar" rev="1451433600" rel="3">
                                30                    </a>
                            <a href="" class="calendar" rev="1451520000" rel="3">
                                31                    </a>
                        </div>
                        <div class="gz-cal-program" id="gz-cal-program-4">
                            <a href="" class="calendar" rev="1446336000" rel="4">
                                01                    </a>
                            <a href="" class="calendar" rev="1446422400" rel="4">
                                02                    </a>
                            <a href="" class="calendar" rev="1446508800" rel="4">
                                03                    </a>
                            <a href="" class="calendar" rev="1446595200" rel="4">
                                04                    </a>
                            <a href="" class="calendar" rev="1446681600" rel="4">
                                05                    </a>
                            <a href="" class="calendar" rev="1446768000" rel="4">
                                06                    </a>
                            <a href="" class="calendar" rev="1446854400" rel="4">
                                07                    </a>
                            <a href="" class="calendar" rev="1446940800" rel="4">
                                08                    </a>
                            <a href="" class="calendar" rev="1447027200" rel="4">
                                09                    </a>
                            <a href="" class="calendar" rev="1447113600" rel="4">
                                10                    </a>
                            <a href="" class="calendar" rev="1447200000" rel="4">
                                11                    </a>
                            <a href="" class="calendar" rev="1447286400" rel="4">
                                12                    </a>
                            <a href="" class="calendar" rev="1447372800" rel="4">
                                13                    </a>
                            <a href="" class="calendar" rev="1447459200" rel="4">
                                14                    </a>
                            <a href="" class="calendar" rev="1447545600" rel="4">
                                15                    </a>
                            <a href="" class="calendar" rev="1447632000" rel="4">
                                16                    </a>
                            <a href="" class="calendar" rev="1447718400" rel="4">
                                17                    </a>
                            <a href="" class="calendar" rev="1447804800" rel="4">
                                18                    </a>
                            <a href="" class="calendar" rev="1447891200" rel="4">
                                19                    </a>
                            <a href="" class="calendar" rev="1447977600" rel="4">
                                20                    </a>
                            <a href="" class="calendar" rev="1448064000" rel="4">
                                21                    </a>
                            <a href="" class="calendar" rev="1448150400" rel="4">
                                22                    </a>
                            <a href="" class="calendar" rev="1448236800" rel="4">
                                23                    </a>
                            <a href="" class="calendar" rev="1448323200" rel="4">
                                24                    </a>
                            <a href="" class="calendar" rev="1448409600" rel="4">
                                25                    </a>
                            <a href="" class="calendar" rev="1448496000" rel="4">
                                26                    </a>
                            <a href="" class="calendar" rev="1448582400" rel="4">
                                27                    </a>
                            <a href="" class="calendar" rev="1448668800" rel="4">
                                28                    </a>
                            <a href="" class="calendar" rev="1448755200" rel="4">
                                29                    </a>
                            <a href="" class="calendar" rev="1448841600" rel="4">
                                30                    </a>
                            <a href="" class="calendar" rev="1448928000" rel="4">
                                01                    </a>
                            <a href="" class="calendar" rev="1449014400" rel="4">
                                02                    </a>
                            <a href="" class="calendar" rev="1449100800" rel="4">
                                03                    </a>
                            <a href="" class="calendar" rev="1449187200" rel="4">
                                04                    </a>
                            <a href="" class="calendar" rev="1449273600" rel="4">
                                05                    </a>
                            <a href="" class="calendar" rev="1449360000" rel="4">
                                06                    </a>
                            <a href="" class="calendar" rev="1449446400" rel="4">
                                07                    </a>
                            <a href="" class="calendar" rev="1449532800" rel="4">
                                08                    </a>
                            <a href="" class="calendar" rev="1449619200" rel="4">
                                09                    </a>
                            <a href="" class="calendar" rev="1449705600" rel="4">
                                10                    </a>
                            <a href="" class="calendar" rev="1449792000" rel="4">
                                11                    </a>
                            <a href="" class="calendar" rev="1449878400" rel="4">
                                12                    </a>
                            <a href="" class="calendar" rev="1449964800" rel="4">
                                13                    </a>
                            <a href="" class="calendar" rev="1450051200" rel="4">
                                14                    </a>
                            <a href="" class="calendar" rev="1450137600" rel="4">
                                15                    </a>
                            <a href="" class="calendar" rev="1450224000" rel="4">
                                16                    </a>
                            <a href="" class="calendar" rev="1450310400" rel="4">
                                17                    </a>
                            <a href="" class="calendar" rev="1450396800" rel="4">
                                18                    </a>
                            <a href="" class="calendar" rev="1450483200" rel="4">
                                19                    </a>
                            <a href="" class="calendar" rev="1450569600" rel="4">
                                20                    </a>
                            <a href="" class="calendar" rev="1450656000" rel="4">
                                21                    </a>
                            <a href="" class="calendar" rev="1450742400" rel="4">
                                22                    </a>
                            <a href="" class="calendar" rev="1450828800" rel="4">
                                23                    </a>
                            <a href="" class="calendar" rev="1450915200" rel="4">
                                24                    </a>
                            <a href="" class="calendar" rev="1451001600" rel="4">
                                25                    </a>
                            <a href="" class="calendar" rev="1451088000" rel="4">
                                26                    </a>
                            <a href="" class="calendar" rev="1451174400" rel="4">
                                27                    </a>
                            <a href="" class="calendar" rev="1451260800" rel="4">
                                28                    </a>
                            <a href="" class="calendar" rev="1451347200" rel="4">
                                29                    </a>
                            <a href="" class="calendar" rev="1451433600" rel="4">
                                30                    </a>
                            <a href="" class="calendar" rev="1451520000" rel="4">
                                31                    </a>
                        </div>
                        <div class="gz-cal-program" id="gz-cal-program-5">
                            <a href="" class="calendarReserved" rev="1446336000" rel="5">
                                01                    </a>
                            <a href="" class="calendarReserved" rev="1446422400" rel="5">
                                02                    </a>
                            <a href="" class="calendarReserved" rev="1446508800" rel="5">
                                03                    </a>
                            <a href="" class="calendarReserved" rev="1446595200" rel="5">
                                04                    </a>
                            <a href="" class="calendarReserved" rev="1446681600" rel="5">
                                05                    </a>
                            <a href="" class="calendarReserved" rev="1446768000" rel="5">
                                06                    </a>
                            <a href="" class="calendarReserved" rev="1446854400" rel="5">
                                07                    </a>
                            <a href="" class="calendarReserved" rev="1446940800" rel="5">
                                08                    </a>
                            <a href="" class="calendarReserved" rev="1447027200" rel="5">
                                09                    </a>
                            <a href="" class="calendarReserved" rev="1447113600" rel="5">
                                10                    </a>
                            <a href="" class="calendarReserved" rev="1447200000" rel="5">
                                11                    </a>
                            <a href="" class="calendarReserved" rev="1447286400" rel="5">
                                12                    </a>
                            <a href="" class="calendarReserved" rev="1447372800" rel="5">
                                13                    </a>
                            <a href="" class="calendarReserved" rev="1447459200" rel="5">
                                14                    </a>
                            <a href="" class="calendarReserved" rev="1447545600" rel="5">
                                15                    </a>
                            <a href="" class="calendarReserved" rev="1447632000" rel="5">
                                16                    </a>
                            <a href="" class="calendarReserved" rev="1447718400" rel="5">
                                17                    </a>
                            <a href="" class="calendarReserved" rev="1447804800" rel="5">
                                18                    </a>
                            <a href="" class="calendarReserved" rev="1447891200" rel="5">
                                19                    </a>
                            <a href="" class="calendarReserved" rev="1447977600" rel="5">
                                20                    </a>
                            <a href="" class="calendarReserved" rev="1448064000" rel="5">
                                21                    </a>
                            <a href="" class="calendarReserved" rev="1448150400" rel="5">
                                22                    </a>
                            <a href="" class="calendarReserved" rev="1448236800" rel="5">
                                23                    </a>
                            <a href="" class="calendarReserved" rev="1448323200" rel="5">
                                24                    </a>
                            <a href="" class="calendarReserved" rev="1448409600" rel="5">
                                25                    </a>
                            <a href="" class="calendarReserved" rev="1448496000" rel="5">
                                26                    </a>
                            <a href="" class="calendarReserved" rev="1448582400" rel="5">
                                27                    </a>
                            <a href="" class="calendarReserved" rev="1448668800" rel="5">
                                28                    </a>
                            <a href="" class="calendarReserved" rev="1448755200" rel="5">
                                29                    </a>
                            <a href="" class="calendarReservedNightsEnd" rev="1448841600" rel="5">
                                30                    </a>
                            <a href="" class="calendar" rev="1448928000" rel="5">
                                01                    </a>
                            <a href="" class="calendar" rev="1449014400" rel="5">
                                02                    </a>
                            <a href="" class="calendar" rev="1449100800" rel="5">
                                03                    </a>
                            <a href="" class="calendar" rev="1449187200" rel="5">
                                04                    </a>
                            <a href="" class="calendar" rev="1449273600" rel="5">
                                05                    </a>
                            <a href="" class="calendar" rev="1449360000" rel="5">
                                06                    </a>
                            <a href="" class="calendar" rev="1449446400" rel="5">
                                07                    </a>
                            <a href="" class="calendar" rev="1449532800" rel="5">
                                08                    </a>
                            <a href="" class="calendar" rev="1449619200" rel="5">
                                09                    </a>
                            <a href="" class="calendar" rev="1449705600" rel="5">
                                10                    </a>
                            <a href="" class="calendar" rev="1449792000" rel="5">
                                11                    </a>
                            <a href="" class="calendar" rev="1449878400" rel="5">
                                12                    </a>
                            <a href="" class="calendar" rev="1449964800" rel="5">
                                13                    </a>
                            <a href="" class="calendar" rev="1450051200" rel="5">
                                14                    </a>
                            <a href="" class="calendar" rev="1450137600" rel="5">
                                15                    </a>
                            <a href="" class="calendar" rev="1450224000" rel="5">
                                16                    </a>
                            <a href="" class="calendar" rev="1450310400" rel="5">
                                17                    </a>
                            <a href="" class="calendar" rev="1450396800" rel="5">
                                18                    </a>
                            <a href="" class="calendar" rev="1450483200" rel="5">
                                19                    </a>
                            <a href="" class="calendar" rev="1450569600" rel="5">
                                20                    </a>
                            <a href="" class="calendar" rev="1450656000" rel="5">
                                21                    </a>
                            <a href="" class="calendar" rev="1450742400" rel="5">
                                22                    </a>
                            <a href="" class="calendar" rev="1450828800" rel="5">
                                23                    </a>
                            <a href="" class="calendar" rev="1450915200" rel="5">
                                24                    </a>
                            <a href="" class="calendar" rev="1451001600" rel="5">
                                25                    </a>
                            <a href="" class="calendar" rev="1451088000" rel="5">
                                26                    </a>
                            <a href="" class="calendar" rev="1451174400" rel="5">
                                27                    </a>
                            <a href="" class="calendar" rev="1451260800" rel="5">
                                28                    </a>
                            <a href="" class="calendar" rev="1451347200" rel="5">
                                29                    </a>
                            <a href="" class="calendar" rev="1451433600" rel="5">
                                30                    </a>
                            <a href="" class="calendar" rev="1451520000" rel="5">
                                31                    </a>
                        </div>
                        <div class="gz-cal-program" id="gz-cal-program-16">
                            <a href="" class="calendar" rev="1446336000" rel="16">
                                01                    </a>
                            <a href="" class="calendar" rev="1446422400" rel="16">
                                02                    </a>
                            <a href="" class="calendar" rev="1446508800" rel="16">
                                03                    </a>
                            <a href="" class="calendar" rev="1446595200" rel="16">
                                04                    </a>
                            <a href="" class="calendar" rev="1446681600" rel="16">
                                05                    </a>
                            <a href="" class="calendar" rev="1446768000" rel="16">
                                06                    </a>
                            <a href="" class="calendar" rev="1446854400" rel="16">
                                07                    </a>
                            <a href="" class="calendar" rev="1446940800" rel="16">
                                08                    </a>
                            <a href="" class="calendar" rev="1447027200" rel="16">
                                09                    </a>
                            <a href="" class="calendar" rev="1447113600" rel="16">
                                10                    </a>
                            <a href="" class="calendar" rev="1447200000" rel="16">
                                11                    </a>
                            <a href="" class="calendar" rev="1447286400" rel="16">
                                12                    </a>
                            <a href="" class="calendar" rev="1447372800" rel="16">
                                13                    </a>
                            <a href="" class="calendar" rev="1447459200" rel="16">
                                14                    </a>
                            <a href="" class="calendar" rev="1447545600" rel="16">
                                15                    </a>
                            <a href="" class="calendar" rev="1447632000" rel="16">
                                16                    </a>
                            <a href="" class="calendar" rev="1447718400" rel="16">
                                17                    </a>
                            <a href="" class="calendar" rev="1447804800" rel="16">
                                18                    </a>
                            <a href="" class="calendar" rev="1447891200" rel="16">
                                19                    </a>
                            <a href="" class="calendar" rev="1447977600" rel="16">
                                20                    </a>
                            <a href="" class="calendar" rev="1448064000" rel="16">
                                21                    </a>
                            <a href="" class="calendar" rev="1448150400" rel="16">
                                22                    </a>
                            <a href="" class="calendar" rev="1448236800" rel="16">
                                23                    </a>
                            <a href="" class="calendar" rev="1448323200" rel="16">
                                24                    </a>
                            <a href="" class="calendar" rev="1448409600" rel="16">
                                25                    </a>
                            <a href="" class="calendar" rev="1448496000" rel="16">
                                26                    </a>
                            <a href="" class="calendar" rev="1448582400" rel="16">
                                27                    </a>
                            <a href="" class="calendar" rev="1448668800" rel="16">
                                28                    </a>
                            <a href="" class="calendar" rev="1448755200" rel="16">
                                29                    </a>
                            <a href="" class="calendar" rev="1448841600" rel="16">
                                30                    </a>
                            <a href="" class="calendar" rev="1448928000" rel="16">
                                01                    </a>
                            <a href="" class="calendar" rev="1449014400" rel="16">
                                02                    </a>
                            <a href="" class="calendar" rev="1449100800" rel="16">
                                03                    </a>
                            <a href="" class="calendar" rev="1449187200" rel="16">
                                04                    </a>
                            <a href="" class="calendar" rev="1449273600" rel="16">
                                05                    </a>
                            <a href="" class="calendar" rev="1449360000" rel="16">
                                06                    </a>
                            <a href="" class="calendar" rev="1449446400" rel="16">
                                07                    </a>
                            <a href="" class="calendar" rev="1449532800" rel="16">
                                08                    </a>
                            <a href="" class="calendar" rev="1449619200" rel="16">
                                09                    </a>
                            <a href="" class="calendar" rev="1449705600" rel="16">
                                10                    </a>
                            <a href="" class="calendar" rev="1449792000" rel="16">
                                11                    </a>
                            <a href="" class="calendar" rev="1449878400" rel="16">
                                12                    </a>
                            <a href="" class="calendar" rev="1449964800" rel="16">
                                13                    </a>
                            <a href="" class="calendar" rev="1450051200" rel="16">
                                14                    </a>
                            <a href="" class="calendar" rev="1450137600" rel="16">
                                15                    </a>
                            <a href="" class="calendar" rev="1450224000" rel="16">
                                16                    </a>
                            <a href="" class="calendar" rev="1450310400" rel="16">
                                17                    </a>
                            <a href="" class="calendar" rev="1450396800" rel="16">
                                18                    </a>
                            <a href="" class="calendar" rev="1450483200" rel="16">
                                19                    </a>
                            <a href="" class="calendar" rev="1450569600" rel="16">
                                20                    </a>
                            <a href="" class="calendar" rev="1450656000" rel="16">
                                21                    </a>
                            <a href="" class="calendar" rev="1450742400" rel="16">
                                22                    </a>
                            <a href="" class="calendar" rev="1450828800" rel="16">
                                23                    </a>
                            <a href="" class="calendar" rev="1450915200" rel="16">
                                24                    </a>
                            <a href="" class="calendar" rev="1451001600" rel="16">
                                25                    </a>
                            <a href="" class="calendar" rev="1451088000" rel="16">
                                26                    </a>
                            <a href="" class="calendar" rev="1451174400" rel="16">
                                27                    </a>
                            <a href="" class="calendar" rev="1451260800" rel="16">
                                28                    </a>
                            <a href="" class="calendar" rev="1451347200" rel="16">
                                29                    </a>
                            <a href="" class="calendar" rev="1451433600" rel="16">
                                30                    </a>
                            <a href="" class="calendar" rev="1451520000" rel="16">
                                31                    </a>
                        </div>
                    </div>
                </div>            
            </div>          
        </div>
    </div>
</section><!-- /.content -->

<br/>

<script type="text/javascript" >
    //var name = document.getElementById("master_entry");
    //document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("m_menu");
    document.getElementById("m_menu").className = "active";
    var slide_bar_element = document.getElementById("m3_submenu");
    document.getElementById("m3_submenu").className = "active";
</script>

@endsection



