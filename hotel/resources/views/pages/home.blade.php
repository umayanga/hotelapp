@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<!-- Content Header (Page header) -->
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-certificate"></i> Home</a></li>
        <li class="active">Welcome  {{ Auth::user()-> userName }}</li>
    </ol>
</section>
<br/>

<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->

            <div class="small-box bg-green">
                <a href="{{route('room_map.show', $_POST['h_id'])}}" class="small-box-footer">
                    <div class="inner">
                        <h3>
                            {{count($rooms_count)}} </h3>
                        <p>
                            Available Room                    </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-calendar"></i>
                    </div>
                    <!--<a href="{{url('reservation_calender')}}" class="small-box-footer">
                     More info <i class="fa fa-arrow-circle-right"></i>
                    </a>-->
                </a>

            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <a href="{{ route('reservation_page.create')}}" class="small-box-footer">
                    <div class="inner">
                        <h3>
                            {{count($daily_reservation_count) }}                  </h3>
                        <p>
                            Reservations                    </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-home"></i>
                    </div>
                    <!--<a href="{{url('reservation_page')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>-->
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <a href="{{url('hotel_page')}}" class="small-box-footer">
                    <div class="inner">
                        <h3>
                            {{ count($hotel_count)}}                    </h3>
                        <p>
                            Hotel  Details                    </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-clipboard"></i>
                    </div>
                    <!--<a href="{{url('hotel_page')}}" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>-->
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <a href="{{url('guest_page')}}" class="small-box-footer">
                <div class="inner">
                    <h3>
                        {{ count($guess_count)}}                 </h3>
                    <p>
                        Guest Details                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
               <!-- <a href="{{url('guest_page')}}" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>-->
                </a>
            </div>
        </div><!-- ./col -->
    </div>
</section>

<script>
    // document.getElementById("slide_bars").value='trans_slide_bar'';

   // var main_heder_element = document.getElementById("master_entry");
   // document.getElementById("master_entry").className = "active";
</script>



@endsection