<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MaddHatter\LaravelFullcalendar\Event;

class CalendarEvent extends Model implements Event {

    //
    protected $table = 'reservation';
    protected $fillable = [
        //
        'accommodateDate',
        'accommodateCloseDate'
    ];

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart() {
        //dd($this->accommodateDate);
        return $this->accommodateDate;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd() {
        return $this->accommodateCloseDate;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle() {
        //
        return 'test';
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay() {
        //
        return true;
    }   

    /**
     * Get the event's ID
     *
     * @return int|string|null
     */
    public function getId() {
        //
        return $this->id;
    }

    /**
     * Optional FullCalendar.io settings for this event
     *
     * @return array
     */
    public function getEventOptions() {
        //
        return null;
    }

}
