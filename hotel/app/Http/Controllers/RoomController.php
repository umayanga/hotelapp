<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use Request;
use Auth;
use App\Models\Room;
use App\Models\Hotel;
use App\Models\RoomType;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class RoomController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //


        $user_id = Auth::user()->id;
        $hotel_id = DB::select(DB::raw("SELECT hotelId FROM user_role WHERE userId ='+$user_id+';"));
        // dd($hotel_id == []);
        if ($hotel_id != []) {
            $h_id = $hotel_id[0]->hotelId;
            $view_no;
            // dd($hotel_id[0]->hotelId);
            if ($hotel_id[0]->hotelId != null) {
                $rooms = Room::where('hotelId', '=', $h_id)->get();
                $view_no = 1;
            } else {
                $rooms = Room::all();
                $view_no = 2;
            }
        } else {
            $rooms = Room::all();
            $view_no = 2;
        }


        foreach ($rooms as $room) {

            $hotelId = DB::table('hotel')->where('id', $room->hotelId)->value('hotalName');
            $roomTypeId = DB::table('roomType')->where('id', $room->roomTypeId)->value('typeName');

            $room->hotelId = $hotelId;
            $room->roomTypeId = $roomTypeId;
        }


        return view('pages.room_details.room.room_page', compact('rooms', 'view_no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        //$hotels = Hotel::lists('hotalName', 'id');
        $room_types = RoomType::lists('typeName', 'id');

        $user_id = Auth::user()->id;
        $hotel_count = DB::select(DB::raw("SELECT hotelId FROM user_role WHERE userId ='+$user_id+';"));
        $match = ['id' => $hotel_count[0]->hotelId];
        $hotels = Hotel::where($match)->lists('hotalName', 'id');

        return view('pages.room_details.room.room_add', compact('hotels', 'room_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //

        //dd($_POST);
        if ($_POST['room_code'] != NULL) {
            $rooms = Request::all();
            Room::create($rooms);
        }
        return redirect('room');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $room = Room::find($id);

        $hotelId = DB::table('hotel')->where('id', $room->hotelId)->value('hotalName');
        $roomTypeId = DB::table('roomType')->where('id', $room->roomTypeId)->value('typeName');

        $room->hotelId = $hotelId;
        $room->roomTypeId = $roomTypeId;

        return view('pages.room_details.room.room_show', compact('room'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $room = Room::find($id);

        $hotels = Hotel::lists('hotalName', 'id');
        $room_types = RoomType::lists('typeName', 'id');


        return view('pages.room_details.room.room_update', compact('room', 'hotels', 'room_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $roomUpdate = Request::all();
        $room = Room::find($id);
        $room->update($roomUpdate);
        return redirect('room');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        Room::find($id)->delete();
        return redirect('room');
    }

}
