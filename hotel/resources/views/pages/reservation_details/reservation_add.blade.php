@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection


@section('content')

<section class="content-header">
    <h1>Receive Your Dream <small>Room </small></h1>
</section><hr/>

<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">

            {!! Form::open(['url' => 'reservation_page']); !!}

            <legend> Reservation Details </legend>
            <!-- Date range -->
            <div class="form-group">
                <label> Accommodate Date range:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="reservation" name="accom_Date">
                    {!! Form::hidden('accommodateDate', null) !!}
                    {!! Form::hidden('accommodateCloseDate', null) !!}

                </div><!-- /.input group -->
            </div><!-- /.form group -->

            <!-- Date and time range -->
            <div class="form-group">
                <label>Check-In Check-Out  range:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="reservationtime" name="check_in_out">
                    {!! Form::hidden('check_in', null) !!}
                    {!! Form::hidden('check_out', null) !!}

                </div><!-- /.input group -->
            </div><!-- /.form group -->


            <div class="form-group">
                {!! Form::label('no_of_members', 'No of Members'); !!}
                <div class="form-controls">                     
                    {!! Form::number('no_of_members', null, ['class' => 'form-control', 'id' => 'no_of_members', 'placeholder' => 'No: of Members ']); !!}
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('hotelId', 'Hotel Name: '); !!}
                        <div class="form-controls">
                            {!! Form::select('hotelId',$hotels, null, ['class' => 'form-control' ]); !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('roomId', 'Room Code: '); !!}
                        <div class="form-controls">
                            {!! Form::select('roomId',$rooms, null, ['class' => 'form-control' ]); !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('guessId', 'Guest Name : '); !!}
                        <div class="form-controls">
                            {!! Form::select('guestId', $guests, null, ['class' => 'form-control' ]); !!}
                        </div>
                    </div>                          
                </div>
            </div>

            <hr/>


            <div>
                <button type="submit"   class="btn btn-primary"  name="reserve_room_button">Reserve  Room</button>
                <button type="reset" class="btn btn-default">Cancel</button>
            </div>

            <br/>


            <legend> Register new  Guest </legend>

            <div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('guest_name', 'Guest Name: '); !!}
                            <div class="form-controls">                     
                                {!! Form::text('Name', null, ['class' => 'form-control', 'id' => 'Name', 'placeholder' => 'Enter Guest Name']); !!}
                            </div>
                        </div>                                
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('guest_nic', 'NIC :'); !!}
                            <div class="form-controls">                     
                                {!! Form::text('nic', null, ['class' => 'form-control', 'id' => 'nic', 'placeholder' => 'Enter Guest NIC ']); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('email', 'E-mail: '); !!}
                        <div class="form-controls">
                            {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'id1', 'placeholder' => 'Enter Email Address ']); !!}
                        </div>
                    </div>                   
                </div>
            </div>

            <div class="form-group">
                <div class='row'>
                    <div class='col-md-6'>
                        {!! Form::label('telephone', 'Telephone : '); !!}
                        <div class="form-controls">
                            {!! Form::text('telephone', null, ['class' => 'form-control', 'id' => 'id4', 'placeholder' => 'Telephone ']); !!}
                        </div>
                    </div>
                </div>
            </div>

            <hr/>

            <div>
                <button type="submit"   class="btn btn-primary"  name="register_guest_button" >Register Guest</button>
                <button type="reset" class="btn btn-default">Cancel</button>
            </div>

            <br/>

            {!! Form::close() !!}         
        </div>
    </div>
</section>



<script type="text/javascript" >
    // var name = document.getElementById("master_entry");
    // document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("m_menu");
    document.getElementById("m_menu").className = "active";
    var slide_bar_element = document.getElementById("m1_submenu");
    document.getElementById("m1_submenu").className = "active";
</script>


@endsection

