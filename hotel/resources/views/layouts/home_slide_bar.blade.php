<aside class="main-sidebar">

    <section class="sidebar">

        <ul class="sidebar-menu">

            <!--<li id="bd_menu" class="treeview">
                <a href="welcome">
                    <i class="fa fa-certificate"></i> 
                    <span>Hotel Details</span> 
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="bd_submenu1"><a href="{{url('hotel_page')}}"><i class="fa fa-circle-o"></i>Hotel Details</a></li>
                </ul>
            </li>-->


            <li id="m_menu" class="treeview">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>Make  Reservation</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="m1_submenu"><a href="{{url('reservation_page')}}"><i class="fa fa-circle-o"></i>Room Booking Details</a></li>
                    <!--<li id="m2_submenu"><a href="#"><i class="fa fa-circle-o"></i>Room Allocation View</a></li>-->
                    <li id="m3_submenu"><a href="{{url('reservation_calender')}}"><i class="fa fa-circle-o"></i>View Available Rooms</a></li>
                </ul>
            </li>

            <li id="guest_menu"><a href="{{url('guest_page')}}"><i class="fa fa-users"></i>Guest Registration</a></li>


            <li id="room_menu" class="treeview">
                <a href="#">
                    <i class="fa fa-hotel"></i>
                    <span>Room Section</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="rm1_submenu"><a href="{{url('room')}}"><i class="fa fa-circle-o"></i>Room Page</a></li>                
                    <li id="rm2_submenu"><a href="{{url('room_type')}}"><i class="fa fa-circle-o"></i>Room Type Page</a></li>
                    <li id="rm3_submenu"><a href="{{url('room_map')}}"><i class="fa fa-circle-o"></i>Room Map</a></li>

                </ul>
            </li>



            <li id="ane_menu"><a href="{{url('calendar')}}"><i class="fa fa-calendar"></i>Calendar</a></li>

            <!--<li id="fd_menu"><a href="#"><i class="fa fa-usd"></i>Room Rates</a></li>-->
            
            <li id="setting_menu"><a href="{{url('system_user_setting')}}"><i class="fa fa-wrench"></i>Account Setting</a></li>

           <!-- <li id="setting_menu" class="treeview">
                <a href="#">
                    <i class="fa  fa-wrench"></i>
                    <span>System Setting</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="sm1_submenu"><a href="{{url('system_user_setting')}}"><i class="fa fa-circle-o"></i>User Settings</a></li>                
                </ul>
            </li>-->
            

        </ul> 
    </section>

</aside>
