<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    protected $table = 'reservation';
    protected $fillable = [
       //
        'accommodateDate',
        'accommodateCloseDate',
        'check_in',
        'check_out',
        'no_of_members',
        'roomId',
        'guestId',
    ];
}

