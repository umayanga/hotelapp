@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')

<?php
//$myString = $roomDetail;
//$myArray = explode('"', $myString);
//dd($myArray);
?>

<section class="content-header">
    <h1>Room  Details View <small>page </small></h1>
</section>


<br/>

<!-- Main content -->

<section class="content">
    <div class="box box-solid box-info">
        <div class="box-header">
            {!! Form::open(['url' => 'room_map/update_room_state']); !!}
            <h3 class="box-title">Room {{ $s_room_detail[0]->room_code }} Details</h3>
            <a href="javascript:history.go(-1)" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-remove-circle"></span> </a>                
            <a href="{!! url('reservation_page') !!}" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-pencil"></span> </a>

        </div><!-- /.box-header -->
        <div class="box-body"> 
            {!! Form::hidden('room_code', $s_room_detail[0]->room_code) !!}
            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Room Type   : </label>            
                        {{ $s_room_detail[0]->typeName}} 
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Hotel Name   : </label>
                        {{ $s_room_detail[0]->hotalName}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Hotel E-mail : </label>
                        {{ $s_room_detail[0]->email}} 
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Room Accommodate Date : </label>
                        {{ $s_reservations[0]->accommodateDate}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Accommodate Close Date : </label>
                        {{ $s_reservations[0]->accommodateCloseDate}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Check-In Date : </label>
                        {{ $s_reservations[0]->check_in}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Check-Out Date : </label>
                        {{ $s_reservations[0]->check_out}} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>No of Members : </label>
                        {{ $s_reservations[0]->no_of_members}} 
                    </div>
                </div>
            </div>

            <br/>

            <!-- remove calnder view code -->
        </div>

        <div class="box-footer">

            <center>
                <p id='buttons'>               
                    {!! Form::submit('Reservation', ['class' => 'btn btn-success']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp            
                </p>
            </center>
            {!! Form::close() !!}
        </div>
    </div><!-- /.box -->
</section><!-- /.content -->

@section('scripts')
{!! $calendar->script() !!}
@stop


<script type="text/javascript" >
    // var name = document.getElementById("master_entry");
    // document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    document.getElementById("room_menu").className = "active";
    var slide_bar_element = document.getElementById("rm3_submenu");
    document.getElementById("rm3_submenu").className = "active";
</script>

@endsection

