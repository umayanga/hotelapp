@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Hotel Details </h1>
</section>


<br/>

<!-- Main content -->
<section class="content fluid">
    <div class="row">
        <div class="box box-warning">
            <div class="gap">
                <div class="box-body">
                    @if ($view_no == 2)
                    <table id="example1" class="table table-bordered table-striped">
                        <col width='auto'>
                        <col width='100'>

                        <thead>
                            <tr>
                                <th>Hotel Name</th>
                                <th><p id='buttons'> <a href="{{ route('hotel_page.create')}}" class="btn btn-success"> <strong> Hotels &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p></th>
                        </tr>
                        </thead>
                        <tbody>                  
                            @foreach($hotels as $hotel)
                            <tr>
                                <td><a href="{{route('hotel_page.show', $hotel->id)}}"> {{ $hotel->hotalName }} </a></td>
                                <td align='center'>
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['hotel_page.destroy',$hotel->id]]) !!}
                                    <a href="{{route('hotel_page.edit',$hotel->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}
                                </td> 
                            </tr>
                            @endforeach

                        </tbody>
                        
                    </table>
                    @else
                    <table id="example1" class="table table-bordered table-striped">                      
                        <col width='auto'>
                        <col width='100'>

                        <thead>
                            <tr>                          
                                <th>Hotel E-mail</th>
                                <th><p id='buttons'> <a href="{{ route('hotel_page.create')}}" class="btn btn-success"> <strong> Hotels &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p></th>
                        </tr>
                        </thead>
                        <tbody>                  
                            @foreach($hotels as $hotel)
                            <tr>
                                <td><a href="{{route('hotel_page.show', $hotel->id)}}"> {{ $hotel->email }} </a></td>        

                                <td align='center'>
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['hotel_page.destroy',$hotel->id]]) !!}
                                    <a href="{{route('hotel_page.edit',$hotel->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}
                                </td> 
                            </tr>

                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>                            
                                <th>Hotel E-mail</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >
   // var name = document.getElementById("master_entry");
   // document.getElementById("master_entry").className = "active";
   // var slide_bar_element = document.getElementById("bd_menu");
   // document.getElementById("bd_menu").className = "active";
   // var slide_bar_element = document.getElementById("bd_submenu1");
   // document.getElementById("bd_submenu1").className = "active";
</script>

@endsection

