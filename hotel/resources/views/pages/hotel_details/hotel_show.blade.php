@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')



<section class="content-header">
    <h1>Hotel  details view <small> page </small></h1>
</section>


<br/>
<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">
            {!! Form::open(['method' => 'DELETE', 'route' => ['hotel_page.destroy', $hotel->id]]); !!}
            <a href="javascript:history.go(-1)" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-remove-circle"></span> </a>
           <!-- <button type="submit" class="btn btn-default" onclick="return confirm('Are you sure?')" style='float: right;'> <span class="glyphicon glyphicon-trash"></span> </button>-->
            <a href="{!! url('hotel_page/'.$hotel->id.'/edit') !!}" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-pencil"></span> </a>
            {!! Form::close() !!}

            <br/> <hr/>

            <fieldset>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Hotel Name : </label> <br/>
                            {{ $hotel->hotalName }} 
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Hotel Description: </label> <br/>
                            {{ $hotel->hotalDescription }} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Hotel Address: </label><br/>
                            {{ $hotel->address }} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Telephone: </label>  <br/>                    
                            {{ $hotel->telephone }} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Mobile: </label><br/>
                            {{ $hotel->mobile }} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Hotel E-mail: </label><br/>
                            {{ $hotel->email }} 
                        </div>
                    </div>
                </div>


            </fieldset>
        </div>
    </div>
</section>




<script type="text/javascript" >
    // var name = document.getElementById("master_entry");
    // document.getElementById("master_entry").className = "active";
    //var slide_bar_element = document.getElementById("bd_menu");
    // document.getElementById("bd_menu").className = "active";
    // var slide_bar_element = document.getElementById("bd_submenu1");
    // document.getElementById("bd_submenu1").className = "active";

</script>

@endsection