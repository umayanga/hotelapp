<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';            
            $table->increments('id');
            $table->string('hotalName',100);
            $table->longText('hotalDescription')->nullable();           
            $table->string('address');         
            $table->string('telephone');
            $table->string('mobile');           
            $table->string('email');
                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotel');
    }
}
