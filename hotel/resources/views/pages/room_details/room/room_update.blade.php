@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection


@section('content')



<section class="content-header">
    <h1>Room Details Update <small>page </small></h1>
</section><hr/>


<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">
            {!! Form::model($room ,['method' => 'PATCH','route'=>['room.update',$room->id]]) !!}
            <div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('room_code', 'Room Code: '); !!}
                            <div class="form-controls">
                                {!! Form::text('room_code', null, ['class' => 'form-control', 'id' => 'code', 'placeholder' => 'Enter Room Code  Here']); !!}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('hotelId', 'Hotel Name: '); !!}
                            <div class="form-controls">
                                {!! Form::select('hotelId', $hotels, null, ['class' => 'form-control' ]); !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('roomTypeId', 'Room Type : '); !!}
                            <div class="form-controls">
                                {!! Form::select('roomTypeId', $room_types, null, ['class' => 'form-control' ]); !!}
                            </div>
                        </div>                          
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('roomState', 'Room State: '); !!}
                            <div class="form-controls">
                                {!! Form::select('roomState', ['Available'=>'Available', 'UnAvailable'=>'UnAvailable'], null, ['class' => 'form-control', 'id'=>'roomState']); !!}
                            </div>
                        </div>                          
                    </div>
                </div>


            </div>


            <div class="box-footer">
                <p id='buttons'>
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                    {!! Form::reset('Cancel', ['class' => 'btn btn-default']) !!}
                </p>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</section>

<script type="text/javascript" >
    //var name = document.getElementById("master_entry");
    //document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    document.getElementById("room_menu").className = "active";
    var slide_bar_element = document.getElementById("rm1_submenu");
    document.getElementById("rm1_submenu").className = "active";
</script>

@endsection



