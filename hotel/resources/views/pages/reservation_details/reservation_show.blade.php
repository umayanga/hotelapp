@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')



<section class="content-header">
    <h1>Reservation  details view <small> page </small></h1>
</section>


<br/>
<section class='container-fluid'>
    <div class="box box-warning">
        <div class="box-body">
            {!! Form::open(['method' => 'DELETE', 'route' => ['reservation_page.destroy', $reservation->id]]); !!}
            <a href="javascript:history.go(-1)" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-remove-circle"></span> </a>
            <button type="submit" class="btn btn-default" onclick="return confirm('Are you sure?')" style='float: right;'> <span class="glyphicon glyphicon-trash"></span> </button>
            <a href="{!! url('reservation_page/'.$reservation->id.'/edit') !!}" class="btn btn-default" style='float: right;'> <span class="glyphicon glyphicon-pencil"></span> </a>
            {!! Form::close() !!}

            <br/> <hr/>

            <fieldset>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Room Code: </label> <br/>
                            {{$room->room_code}}
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Guest Name: </label> <br/>
                            {{$guest->Name}}
                        </div>
                    </div>
                </div>
                
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>No of Members : </label><br/>
                            {{ $reservation->no_of_members }} 
                        </div>
                    </div>
                </div>

             
                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Accommodate Date : </label><br/>
                            {{  date('F d, Y', strtotime($reservation->accommodateDate))}} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Accommodate  Close Date :</label>  <br/>                    
                            {{ date('F d, Y', strtotime($reservation->accommodateCloseDate)) }} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Check In Date : </label><br/>
                            {{$reservation->check_in}} 
                        </div>
                    </div>
                </div>

                <br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Check Out Date: </label><br/>
                            {{$reservation->check_out }} 
                        </div>
                    </div>
                </div>


            </fieldset>
        </div>
    </div>
</section>




<script type="text/javascript" >
    // var name = document.getElementById("master_entry");
    // document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("m_menu");
    document.getElementById("m_menu").className = "active";
    var slide_bar_element = document.getElementById("m1_submenu");
    document.getElementById("m1_submenu").className = "active";
</script>

@endsection