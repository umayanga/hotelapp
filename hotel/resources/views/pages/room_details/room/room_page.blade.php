@extends('layouts.app')

@section('slide_bar')
@include('layouts.home_slide_bar')
@endsection

@section('content')




<section class="content-header">
    <h1>Room   Details <small>page </small></h1>
</section>


<br/>

<!-- Main content -->
<section class="content fluid">
    <div class="row">
        <div class="box box-warning">
            <div class="gap">
                <div class="box-body">
                    @if ($view_no == 2)
                    <table id="example1" class="table table-bordered table-striped">
                        <col width='auto'>
                        <col width='auto'>  
                        <col width='100'>

                        <thead>
                            <tr>
                                <th>Hotel Name</th>
                                <th>Room Code</th>
                                <th><p id='buttons'> <a href="{{ route('room.create')}}" class="btn btn-success"> <strong>Rooms &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p></th>
                        </tr>
                        </thead>
                        <tbody>                  
                            @foreach($rooms as $room)
                            <tr>
                                <td><a href="{{route('room.show', $room->id)}}"> {{ $room->hotelId }} </a></td>        
                                <td><a href="{{route('room.show', $room->id)}}"> {{ $room->room_code }} </a></td>                                  
                                <td align='center'>
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['room.destroy',$room->id]]) !!}
                                    <a href="{{route('room.edit',$room->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}
                                </td> 
                            </tr>
                            @endforeach

                        </tbody>                      
                    </table>
                    @else
                    <table id="example1" class="table table-bordered table-striped">                       
                        <col width='auto'>  
                        <col width='100'>

                        <thead>
                            <tr>                       
                                <th>Room Code</th>
                                 <th><p id='buttons'> <a href="{{ route('room.create')}}" class="btn btn-success"> <strong> Rooms &nbsp </strong> <span class="glyphicon glyphicon-plus"></span> </a> </p></th>
                        </tr>
                        </thead>
                        <tbody>                  
                            @foreach($rooms as $room)
                            <tr>
                                <td><a href="{{route('room.show', $room->id)}}"> {{ $room->room_code }} </a></td>                                  
                                <td align='center'>
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['room.destroy',$room->id]]) !!}
                                    <a href="{{route('room.edit',$room->id)}}" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-pencil"></span> </a> &nbsp &nbsp
                                    <button type="submit" class="btn btn-default btn-sm" onclick="return confirm('Are you sure?')"> <span class="glyphicon glyphicon-trash"></span> </button> 
                                    {!! Form::close() !!}
                                </td> 
                            </tr>
                            @endforeach

                        </tbody>                     
                    </table>
                    @endif

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript" >
   // var name = document.getElementById("master_entry");
   // document.getElementById("master_entry").className = "active";
    var slide_bar_element = document.getElementById("room_menu");
    document.getElementById("room_menu").className = "active";
    var slide_bar_element = document.getElementById("rm1_submenu");
    document.getElementById("rm1_submenu").className = "active";
</script>

@endsection

